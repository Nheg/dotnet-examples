﻿using System;

namespace M02.Samples
{
    internal class Program
    {
        public class Panda
        {
            public Panda Mate;

            public void Marry(Panda partner)
            {
                Mate = partner;
                partner.Mate = this;
            }
        }

        static void Main(string[] args)
        {
            new Panda().Marry(new Panda());
        }
    }
}
