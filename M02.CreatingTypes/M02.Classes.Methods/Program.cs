﻿using System;

namespace M02.Classes.Methods
{
    public class IllegalOverloading
    {
        public void Foo(int x) { Console.WriteLine(x); }
        public int Foo(int x) => x * 2; //Compile-time error! Becuase method signature equals

        public void Goo(int[] x) { }
        public void Goo(params int[] x) { } //Compile-time error

        void Hoo(int x) { }
        void Hoo(ref int x) { }      // OK so far
        void Hoo(out int x) { }      // Compile-time error
    }
    public class Overloading
    {
        public void Foo(int x) { Console.WriteLine(x); }                    //1
        public void Foo(double x) { Console.WriteLine(x); }                 //2
        public void Foo(double x, double y) { Console.WriteLine(x * y); }   //3
        public void Foo(double x, int y) { Console.WriteLine(x * y); }      //4
    }
    public class ExpressionBodied
    {
        public int Foo1(int x) { return x * 2; }
        public int Foo2(int x) => x * 2;
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            //Expression-bodied
            var expressionBodied = new ExpressionBodied();
            Console.WriteLine(expressionBodied.Foo1(2));
            Console.WriteLine(expressionBodied.Foo2(2));

            //Overloading method
            var overloading = new Overloading();
            overloading.Foo(1);         //1
            overloading.Foo(1.0);       //2
            overloading.Foo(12.3, 1.0); //3
            overloading.Foo(12.3, 1);   //4

        }
    }
}
