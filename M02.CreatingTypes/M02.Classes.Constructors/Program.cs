﻿using System;

namespace M02.Classes.Constructors
{
    public class MyClass
    {
        MyClass() { }

        public static MyClass Create()
        {
            /*
             * Custom logic here
             */
            return new MyClass();
        }
    }

    public class Wine
    {
        public decimal price;
        public int year;
        public string brand;
        public Wine() { } //Default constructor
        public Wine(string brand) 
        {
            this.brand = brand;
        }
        public Wine(string brand, int year) : this(brand, year, default) { }
        public Wine(string brand, int year, decimal price)
        {
            this.brand = brand;
            this.year = year;
            this.price = price;
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            var wine = new Wine();
            Console.WriteLine($"Wine: {wine.brand}, {wine.year}, {wine.price}");
            
            var wine1 = new Wine("Bourgoundian", 1971);
            Console.WriteLine($"Wine1: {wine1.brand}, {wine1.year}, {wine1.price}");
            
            var wine2 = new Wine("La chateua rouge", 1964, 70000);
            Console.WriteLine($"Wine2: {wine2.brand}, {wine2.year}, {wine2.price}");

            
            var myClass = new MyClass(); //Compile time error
            var myClass1 = MyClass.Create(); 
            
        }
    }
}
