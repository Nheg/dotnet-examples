﻿using System;

namespace M02.Structs
{
    public interface IFoo
    {
        public void Foo();
    }
    public struct Point : IFoo 
    {
        //public int illegalVar = 1; //Compile-time error. Cannot initialize field
        public int X, Y;
        public Point(int x, int y) { X = x; Y = y; }
        // The parameterless constructor is implicit.
        //public Point() { } // Cannot have paramtreless constructor in C# verxsion <= 9.0. In C# 10.0 <= can have.
        public void Foo() { }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            var point = new Point(1, 0);
            Console.WriteLine(point);

            var point1 = new Point { X = 10, Y = 11 };
            Console.WriteLine(point1);

            IFoo point2 = new Point(); // Here we did booxing
            point2.Foo();
        }
    }
}
