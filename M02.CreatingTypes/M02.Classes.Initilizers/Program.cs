﻿using System;

namespace M02.Classes.Initilizers
{
    public class Bunny
    {
        public string Name;
        public bool LikesCarrots;
        public bool LikesHumans;

        public Bunny() { }
        public Bunny(string n) { Name = n; }
        public Bunny(string name, bool likesCarrots = false,
            bool likesHumans = false)
        {
            Name = name;
            LikesCarrots = likesCarrots;
            LikesHumans = likesHumans;
        }

    }

    internal class Program
    {
        static void Main(string[] args)
        {
            // Object initialization syntax. Note that we can still specify constructor arguments:

            Bunny b1 = new Bunny { Name = "Bo", LikesCarrots = true, LikesHumans = false };
            Bunny b2 = new Bunny("Bo") { LikesCarrots = true, LikesHumans = false };
            Bunny b3 = new Bunny(name: "Bo", likesHumans: true); //Also you can use named parametrs like in methods

        }
    }
}
