﻿using System;

namespace M02.Classes.Properties
{
    // Properties look like fields from the outside but internally, they contain logic, like methods:
    public class Stock
    {
        decimal currentPrice;           // The private "backing" field

        public decimal CurrentPrice     // The public property
        {
            get { return currentPrice; }
            set { currentPrice = value; }
        }
    }

    public class Stock1
    {
        decimal currentPrice;           // The private "backing" field
        public decimal CurrentPrice     // The public property
        {
            get { return currentPrice; }
            set { currentPrice = value; }
        }

        decimal sharesOwned;           // The private "backing" field
        public decimal SharesOwned     // The public property
        {
            get { return sharesOwned; }
            set { sharesOwned = value; }
        }

        public decimal Worth
        {
            get { return currentPrice * sharesOwned; }
        }
        //public decimal Worth => currentPrice * sharesOwned; // Also work like a method
    }

    // Here's the preceding example rewritten with two automatic properties:

    public class AutoStock
    {
        public decimal CurrentPrice { get; set; }   // Automatic property
        public decimal SharesOwned { get; set; }   // Automatic property

        public decimal Worth
        {
            get { return CurrentPrice * SharesOwned; }
        }
    }


    internal class Program
    {
        static void Main(string[] args)
        {
            var stock = new Stock();
            stock.CurrentPrice = 123.45M;
            Console.WriteLine(stock.CurrentPrice);

            var stock2 = new Stock { CurrentPrice = 83.12M };
            Console.WriteLine(stock.CurrentPrice);

            var stock1 = new Stock1 { CurrentPrice = 50, SharesOwned = 100 };
            Console.WriteLine(stock1.Worth);

            var autoStock = new AutoStock { CurrentPrice = 50, SharesOwned = 100 };
            Console.WriteLine(autoStock.Worth);
        }
    }
}
