﻿using System;

namespace M02.Classes
{
    class Octopus
    {
        public readonly string name;
        //string name;
        public int age = 10;
        public readonly int legs = 8, eyes = 1;
        public Octopus(string name)
        {
            this.name = name;
        }
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            var octupus = new Octopus("Octopus");
            Console.WriteLine(octupus.name);
            octupus.name = "Hhha"; // Compile time error
        }
    }
}
